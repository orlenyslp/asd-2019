defmodule Takso.ApiAuthPipeline do
  use Guardian.Plug.Pipeline,
      otp_app: :takso,
      error_handler: Takso.ApiErrorHandler,
      module: Takso.Guardian

    plug Guardian.Plug.VerifyHeader, realm: "Bearer"
    plug Guardian.Plug.EnsureAuthenticated
    plug Guardian.Plug.LoadResource
end
