defmodule TaksoWeb.BookingAPIController do
  use TaksoWeb, :controller

  import Ecto.Query, only: [from: 2]
  alias Takso.{Repo, Sales.Taxi}

  def create(conn, params) do
    query = from t in Taxi, where: t.status == "available", select: t
    available_taxis = Repo.all(query)
    if length(available_taxis) > 0 do
      taxi = List.first(available_taxis)
      TaksoWeb.Endpoint.broadcast("driver:lobby", "requests", params |> Map.put(:booking_id, 1))
      conn
      |> put_status(201)
      |> json(%{msg: "We are processing your request"})
    else
      conn
      |> put_status(406)
      |> json(%{msg: "Our apologies, we cannot serve your request in this moment"})
    end
  end

  def update(conn, _params) do
    TaksoWeb.Endpoint.broadcast("customer:lobby", "requests", %{msg: "Your taxi will arrive in 5 minutes"})
    conn
    |> put_status(200)
    |> json(%{msg: "Notification sent to the customer"})
  end

end
